﻿using System;

using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Threading;

namespace CDSuite
{
	/// <summary>
	/// The actuall server.
	/// </summary>
	public sealed class Server : IAsyncServer
	{

		#region [OBSOLETE]The server is only accessable through our local instance of Server().

		//private static readonly IAsyncServer instance = new Server();

		///// <summary>
		///// CStor.
		///// </summary>
		//private Server()
		//{
		//}

		///// <summary>
		///// This is the only way to get an instance of Server()
		///// </summary>
		//public static IAsyncServer Instance
		//{
		//	get
		//	{
		//		return instance;
		//	}
		//}

		#endregion

		/// <summary>
		/// CStor.
		/// </summary>
		public Server()
		{
			//
		}


		/// <summary>
		/// Delimeter used for splitting messages from commands.
		/// </summary>
		public string Delimeter = "||";



		#region Locals


		private AsyncCallback _WaitForClientCallBack;
		private Socket _serverListeningSocket;

		/// <summary>
		///  Used to keep track of worker sockets for each connected client. It's a synchronized ArrayList for thread safety.
		/// </summary>
		private System.Collections.ArrayList _socketclientList = ArrayList.Synchronized(new System.Collections.ArrayList());

		
		/// <summary>
		/// Keep track of the cumulative total number of clients who have connected during this servers up time.
		/// Since multiple threads can access this variable, modifying this variable should be done in a thread safe manner (Interlock)
		/// </summary>
		private int _cumulativeclientCount = 0;

		/// <summary>
		/// Flag for is the server in a listening state.
		/// </summary>
		private bool _arewelistening = false;



		private Hashtable _clientstates = new Hashtable();


		#endregion



		#region IAsyncServer Members


		/// <summary>
		/// Event fired when we've received data from a socket client.
		/// </summary>
		public event DataReceivedHandler DataReceived;
		/// <summary>
		/// Event fired when a socket client connects.
		/// </summary>
		public event ClientConnectedHandler ClientConnected;
		/// <summary>
		/// Event fired when a socket client disconnects.
		/// </summary>
		public event ClientDisconnectedHandler ClientDisconnected;
		/// <summary>
		/// Event fired when the server starts listening for socket clients.
		/// </summary>
		public event ServerConnectedHandler ServerConnected;
		/// <summary>
		/// Event fired when the server stops listening for socket clients. Means we are not accepting any new client connections.
		/// </summary>
		public event ServerDisconnectedHandler ServerDisconnected;
		/// <summary>
		/// Event fired when we've retrieved the IPs of our local host.
		/// </summary>
		public event IPsRetrievedHandler IPsReceived;



		/// <summary>
		/// Gets a list of available IPs on this host.
		/// </summary>
		public void GetIPs()
		{
			IPHostEntry localhost = Dns.GetHostEntry(Dns.GetHostName());
			ArrayList ips = new ArrayList();

			// add the available ips to the list
			foreach (IPAddress ip in localhost.AddressList)
			{
				ips.Add(ip.ToString());

			}

			// add loopback
			ips.Add("127.0.0.1");

			// notify subscribers
			if (IPsReceived != null)
				IPsReceived.Invoke(ips);
		}


		/// <summary>
		/// Callback function which will be invoked when the socket detects any client writing data on the stream.
		/// </summary>
		/// <param name="asyn">asyn.AsyncState holds a reference to a ClientState object.</param>
		public void OnDataReceived(IAsyncResult asyn)
		{
			ClientState client = (ClientState)asyn.AsyncState;
			try
			{
				// return the number of characters written to the stream by the socket client
				int clientmessagesize = client.CurrentSocketClient.EndReceive(asyn);

				// Extract the characters as a buffer
				char[] clientmessagebuffer = new char[clientmessagesize + 1];
				System.Text.Decoder decoder = System.Text.Encoding.UTF8.GetDecoder();
				int charLen = decoder.GetChars(client.CurrentSocketDataBuffer, 0, clientmessagesize, clientmessagebuffer, 0);
				string clientsmessage = new String(clientmessagebuffer);
				// strip any residue
				clientsmessage = clientsmessage.Replace("\r\n\0", "");

				// let subscriber know we just got a message from a socket client
				if (DataReceived != null)
					DataReceived.Invoke(client.CurrentSocketClientID, clientsmessage);

				// Continue waiting for data on the clients Socket
				WaitForDataFromClient(client.CurrentSocketClient, client);
			}
			catch (Exception ex)
			{
				if (_socketclientList.Count < 1)
					return;

				// Remove the reference to the worker socket
				_socketclientList[client.CurrentSocketClientID - 1] = null;

				// raise event to notify of socket client disconnect
				if (ClientDisconnected != null)
					ClientDisconnected.Invoke(client.CurrentSocketClientID);
			}

		}



		/// <summary>
        /// Start the servers listener socket.
		/// </summary>
		/// <param name="listenerEndPoint">The IP4 EndPoint to listen on.</param>
		public void StartListeningForClients(IPEndPoint listenerEndPoint)
		{
			// Create the listening socket using IP4
			_serverListeningSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
			// Bind to the address
			_serverListeningSocket.Bind(listenerEndPoint);
			// Start listening
			_serverListeningSocket.Listen(100);
			// Create the call back for any socket client connections
			_serverListeningSocket.BeginAccept(new AsyncCallback(OnClientConnect), null);

			// notify client that we are listening
			if(ServerConnected != null)
				ServerConnected.Invoke();

			_arewelistening = true;
		}



		/// <summary>
		/// Determines if the server is in a listening state.
		/// </summary>
		/// <returns>True if we are listening, otherwise False.</returns>
		public bool AreWeListening()
		{
			return _arewelistening;
		}

		/// <summary>
		/// Stop the servers listener socket. Does not disconnect currently connected socket clients.
		/// </summary>
		public void StopListeningForClients()
		{
			if (_serverListeningSocket != null)
			{
				_serverListeningSocket.Close();
			}

			// let subscriber know we stopped listening for new socket clients
			if (ServerDisconnected != null)
				ServerDisconnected.Invoke();

			_arewelistening = false;
		}



		/// <summary>
		/// Check if a socket client is connected.
		/// </summary>
		/// <param name="clientID">The ID of the client.</param>
		/// <returns>True if connected, otherwise False.</returns>
		public bool IsClientConnected(int clientID)
		{
			if (_socketclientList.Count < 1)
				return false;

			try
			{
				Socket workerSocket = (Socket)_socketclientList[clientID];
				if (workerSocket != null)
				{
					if (workerSocket.Connected)
					{
						return true;
					}
				}
			}
			catch { }


			return false;
		}

		/// <summary>
		/// Callback function which will be invoked when a socket client connects.
		/// </summary>
		/// <param name="asyn">asyn.AsyncState holds a reference to a Socket object.</param>
		public void OnClientConnect(IAsyncResult asyn)
		{
			try
			{
				// complete/end the async call which returns the reference to a Socket object
				Socket workerSocket = _serverListeningSocket.EndAccept(asyn);

				// increment the cumulative client count in a thread safe manner
				Interlocked.Increment(ref _cumulativeclientCount);

				// Add the workerSocket reference to our ArrayList
				_socketclientList.Add(workerSocket);

				// Send a welcome message to the socket client
				ClientMessage messagetoclient = new ClientMessage();
				messagetoclient.Command = "WELCOME";
				messagetoclient.Delimeter = this.Delimeter.ToString();
				messagetoclient.MessageToClient = _cumulativeclientCount.ToString();
			
				SendMessageToClient(messagetoclient, _cumulativeclientCount);

				// let subscriber know a new socket client connected
				if(ClientConnected != null)
					ClientConnected.Invoke(_cumulativeclientCount);

				// create an initial ClientState for the socket client.
				ClientState clientstate = new ClientState(workerSocket, _cumulativeclientCount);
				clientstate.CurrentSocketClientID = _cumulativeclientCount;
				_clientstates.Add(_cumulativeclientCount, clientstate);

				// Let the worker socket listen for the just connected socket client
				WaitForDataFromClient(workerSocket, clientstate);

				// Since the listener socket is now free, it can go back and wait for other clients who are attempting to connect
				_serverListeningSocket.BeginAccept(new AsyncCallback(OnClientConnect), null);
			}
			catch (SocketException se)
			{
				// ignore
				//throw se;
			}

		}



		/// <summary>
		/// Start waiting for data from the socket client.
		/// </summary>
		/// <param name="clientsocket"></param>
		/// <param name="clientstate"></param>
		public void WaitForDataFromClient(Socket clientsocket, ClientState clientstate)
		{
			try
			{
				if (_WaitForClientCallBack == null)
				{
					// callback function for any write activity by the connected socket client
					_WaitForClientCallBack = new AsyncCallback(OnDataReceived);
				}
				
				// start the async operation including the socket clients ClientState
				clientsocket.BeginReceive(clientstate.CurrentSocketDataBuffer, 0, clientstate.CurrentSocketDataBuffer.Length, SocketFlags.None, _WaitForClientCallBack, clientstate);
			}
			catch (SocketException se)
			{
				throw se;
			}
		}

		/// <summary>
		/// Sends a message to the specified socket client.
		/// </summary>
		/// <param name="messagetoclient">The message to send to the socket client.</param>
		/// <param name="clientID">The ID of the client to send the message to.</param>
		public void SendMessageToClient(ClientMessage messagetoclient, int clientID)
		{
			if (_socketclientList.Count < 1)
				return;

			Socket clientsSocket = (Socket)_socketclientList[clientID - 1];
			if (clientsSocket != null)
			{
				if (clientsSocket.Connected)
				{
					try
					{
						NetworkStream networkStream = new NetworkStream(clientsSocket);
						System.IO.StreamWriter streamWriter = new System.IO.StreamWriter(networkStream);
						streamWriter.WriteLine(messagetoclient.ToString());
						streamWriter.Flush();
					}
					catch { }
				}
			}

		}


		/// <summary>
		/// Sends a message to all connected socket clients.
		/// </summary>
		/// <param name="messagetoclient">The message to send to the clients.</param>
		public void SendBroadcastMessage(ClientMessage messagetoclient)
		{
			if (_socketclientList.Count < 1)
				return;

			for (int clientID = 0; clientID < _socketclientList.Count; clientID++)
			{
				Socket clientsSocket = (Socket)_socketclientList[clientID];
				if (clientsSocket != null)
				{
					if (clientsSocket.Connected)
					{
						try
						{
							NetworkStream networkStream = new NetworkStream(clientsSocket);
							System.IO.StreamWriter streamWriter = new System.IO.StreamWriter(networkStream);
							streamWriter.WriteLine(messagetoclient.ToString());
							streamWriter.Flush();
						}
						catch { }
					}
				}
			}

		}


		/// <summary>
		/// Disconnects all currently connected socket clients.
		/// </summary>
		public void DisconnectAllClients()
		{
			if (_socketclientList.Count < 1)
				return;

			Socket workerSocket = null;
			for (int i = 0; i < _socketclientList.Count; i++)
			{
				workerSocket = (Socket)_socketclientList[i];
				if (workerSocket != null)
				{
					workerSocket.Close();
					workerSocket = null;
				}
			}

			_socketclientList.Clear();
		}


		/// <summary>
		/// Gets the ClientState of currently connected clients.
		/// </summary>
		/// <returns></returns>
		public ArrayList GetConnectedClientList()
		{
			ArrayList connectedclients = new ArrayList();

			//listBoxClientList.Items.Clear();
			for (int clientID = 0; clientID < _socketclientList.Count; clientID++)
			{
				Socket workerSocket = (Socket)_socketclientList[clientID];
				if (workerSocket != null)
				{
					if (workerSocket.Connected)
					{
						// get the socket clients state
						ClientState currentclientstate = (ClientState)_clientstates[clientID + 1];
						connectedclients.Add(currentclientstate);
					}
				}
			}

			return connectedclients;
		}


		/// <summary>
		/// Updates the ClientState of a particular socket client. Cannot update the socket or client ID.
		/// </summary>
		/// <param name="newclientstate">The ClientState oblect that holds the changes.</param>
		/// <param name="clientID">The ID of the client to send the message to.</param>
		public void UpdateClientState(ClientState newclientstate, int clientID)
		{
			try
			{
				// get the socket clients state
				ClientState currentclientstate = (ClientState)_clientstates[clientID];
				// now update these fields
				currentclientstate.Channel = newclientstate.Channel;
				currentclientstate.Nickname = newclientstate.Nickname;
			}
			finally
			{ }
		}




		#endregion

		#region IDisposable Members

		/// <summary>
		/// Cleanup anything left over.
		/// </summary>
		public void Dispose()
		{
			DisconnectAllClients();
			StopListeningForClients();
		}

		#endregion




	}	// end class Server

}	// end namespace CDSuite
