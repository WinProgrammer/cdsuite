﻿using System;
using System.Collections;
using System.Net;
using System.Windows.Forms;

namespace CDSuite
{
	/// <summary>
	/// 
	/// </summary>
	public partial class frmServer : Form
	{
		Server _server = null;

		// This delegate enables async calls for setting the text property of txtReceive
		delegate void SetTextCallback(string text);

		// If the calling thread is different from the thread that created the TextBox control, this method creates a
		// SetTextCallback and calls itself asynchronously using the Invoke method.
		//
		// If the calling thread is the same as the thread that created the TextBox control, the Text property is set directly. 
		private void SetText(string text)
		{
			// InvokeRequired required compares the thread ID of the calling thread to the thread ID of the creating thread.
			// If these threads are different, it returns true.
			if (this.txtReceive.InvokeRequired)
			{
				SetTextCallback d = new SetTextCallback(SetText);
				this.Invoke(d, new object[] { text });
			}
			else
			{
				this.txtReceive.Text += text;
			}
		}



		ClientQueue _clientqueue = null;


		/// <summary>
		/// CStor.
		/// </summary>
		public frmServer()
		{
			InitializeComponent();
		}

		private void frmServer_Load(object sender, EventArgs e)
		{
			//
		}

		private void btnStartListening_Click(object sender, EventArgs e)
		{
			if (null == _server)
			{
				//_server = (Server)Server.Instance;
				_server = new Server();

				_server.DataReceived += _server_DataReceived;
				_server.ClientConnected += _server_ClientConnected;
				_server.ClientDisconnected += _server_ClientDisconnected;
				_server.ServerConnected += _server_ServerConnected;
				_server.ServerDisconnected += _server_ServerDisonnected;
				_server.IPsReceived += _server_IPsReceived;
			}

			IPAddress ia = IPAddress.Parse(cboIPs.SelectedItem.ToString());
			IPEndPoint iep = new IPEndPoint(ia, int.Parse(txtPort.Text));

			_clientqueue = new ClientQueue();
			_clientqueue.ItemEnqueued += _clientqueue_ItemEnqueued;

			_server.StartListeningForClients(iep);
		}

		void _clientqueue_ItemEnqueued()
		{
			Console.WriteLine("ITEM ENQUEUED");
			//Object o = _clientqueue.Dequeue();

			ProcessMessage();

			//throw new NotImplementedException();
		}

		/// <summary>
		/// This processes a queued message from a socket client.
		/// </summary>
		private void ProcessMessage()
		{
			string[] completemsg = (string[]) _clientqueue.Dequeue();

			Console.WriteLine("ITEM DEQUEUED");

			int clientID = int.Parse(completemsg[0]);
			string msgfromsocketclient = completemsg[1];


			// decode the message from the client
			string[] tmp = msgfromsocketclient.Split(new string[] { _server.Delimeter }, StringSplitOptions.None);
			string cmdfromclient = tmp[0];
			string msgfromclient = "";

			if (tmp.GetLength(0) > 1)
				msgfromclient = tmp[1];

			ClientMessage msgtoclient;

			// now process the command from the client
			switch (cmdfromclient)
			{
				case "COMCHECK":
					// comcheck
					msgtoclient = new ClientMessage();
					msgtoclient.Command = "COMCHECK";
					msgtoclient.Delimeter = _server.Delimeter;
					msgtoclient.MessageToClient = "COMCHECK";

					_server.SendMessageToClient(msgtoclient, clientID);

					break;

				case "CHANNEL":
					SetText(clientID.ToString() + ": Setting channel to: " + msgfromclient + "\r\n");

					// make any changes to the socket clients channel
					ClientState updatedclientstate = new ClientState(null, -1);
					updatedclientstate.Channel = msgfromclient;
					_server.UpdateClientState(updatedclientstate, clientID);


					// we got the channel now ask for Nickname
					msgtoclient = new ClientMessage();
					msgtoclient.Command = "NICKNAME";
					msgtoclient.Delimeter = _server.Delimeter;
					msgtoclient.MessageToClient = "";

					_server.SendMessageToClient(msgtoclient, clientID);

					break;

				case "NICKNAME":
					SetText(clientID.ToString() + ": Setting nickname to: " + msgfromclient + "\r\n");

					// make any changes to the socket clients nickname
					ClientState updatenickname = new ClientState(null, -1);
					updatenickname.Nickname = msgfromclient;
					_server.UpdateClientState(updatenickname, clientID);

					break;

				case "SAY":
					SetText(clientID.ToString() + ": Says: " + msgfromclient + "\r\n");

					break;

				default:
					break;
			}
		}


		void _server_IPsReceived(System.Collections.ArrayList ips)
		{
			cboIPs.Items.Clear();

			foreach (string ip in ips)
			{
				cboIPs.Items.Add(ip);
			}

			cboIPs.SelectedIndex = 0;
		}

		void _server_ServerDisonnected()
		{
			SetText("Server stopped listening\r\n");
			btnStartListening.Enabled = true;
			btnStopListening.Enabled = false;
		}

		void _server_ServerConnected()
		{
			SetText("Server is listening\r\n");
			btnStartListening.Enabled = false;
			btnStopListening.Enabled = true;
		}

		void _server_ClientDisconnected(int clientID)
		{
			SetText(clientID.ToString() + ": Disconnected\r\n");
		}

		void _server_ClientConnected(int clientID)
		{
			SetText(clientID.ToString() + ": Connected\r\n");

			// request the clients channel
			if (_server == null)
				return;

			ClientMessage msg = new ClientMessage();
			msg.Command = "CHANNEL";
			msg.Delimeter = _server.Delimeter.ToString();
			msg.MessageToClient = "";
			
			_server.SendMessageToClient(msg, clientID);
		}


		/// <summary>
		/// we've gotten data from a socket client
		/// </summary>
		/// <param name="clientID">The ID of the socket client.</param>
		/// <param name="msgfromsocketclient">The message from the socket client.</param>
		void _server_DataReceived(int clientID, string msgfromsocketclient)
		{
			//SetText(clientID.ToString() + ": " + msgfromsocketclient + "\r\n");
			_clientqueue.Enqueue(new string[] { clientID.ToString(), msgfromsocketclient });

			//// decode the message
			//string[] tmp = msgfromsocketclient.Split(new string[] { _server.Delimeter }, StringSplitOptions.None);
			//string cmdfromclient = tmp[0];
			//string msgfromclient = "";

			//if(tmp.GetLength(0) > 1)
			//	msgfromclient = tmp[1];

			//switch (cmdfromclient)
			//{ 
			//	case "CHANNEL":
			//		SetText(clientID.ToString() + ": Setting channel to: " + msgfromclient + "\r\n");

			//		// make any changes to the socket clients channel
			//		ClientState updatedclientstate = new ClientState(null, -1);
			//		updatedclientstate.Channel = msgfromclient;
			//		_server.UpdateClientState(updatedclientstate, clientID);


			//		// now ask for Nickname
			//		ClientMessage msgtoclient = new ClientMessage();
			//		msgtoclient.Command = "NICKNAME";
			//		msgtoclient.Delimeter = _server.Delimeter;
			//		msgtoclient.MessageToClient = "";

			//		_server.SendMsgToClient(msgtoclient, clientID);

			//		break;

			//	case "NICKNAME":
			//		// make any changes to the socket clients nickname
			//		ClientState updatenickname = new ClientState(null, -1);
			//		updatenickname.Nickname = msgfromclient;
			//		_server.UpdateClientState(updatenickname, clientID);

			//		break;

			//	case "SAY":
			//		SetText(clientID.ToString() + ": Says: " + msgfromclient + "\r\n");

			//		break;

			//	default:
			//		break;
			//}
		}

		private void btnStopListening_Click(object sender, EventArgs e)
		{
			if (_server == null)
				return;
			_clientqueue.Clear();

			_server.StopListeningForClients();

			_clientqueue = null;
		}

		private void btnSendMessage_Click(object sender, EventArgs e)
		{
			if (_server == null)
				return;

			ClientMessage msg = new ClientMessage();
			msg.Command = "SAY";
			msg.Delimeter = _server.Delimeter;
			msg.MessageToClient = txtMessage.Text;

			_server.SendMessageToClient(msg, int.Parse(txtClientID.Text));
		}

		private void btnSendBroadcastMessage_Click(object sender, EventArgs e)
		{
			if (_server == null)
				return;

			ClientMessage msg = new ClientMessage();
			msg.Command = "SAY";
			msg.Delimeter = _server.Delimeter;
			msg.MessageToClient = txtMessage.Text;


			_server.SendBroadcastMessage(msg);
		}

		private void btnDisconnectAllClients_Click(object sender, EventArgs e)
		{
			if (_server == null)
				return;

			_server.DisconnectAllClients();
		}

		private void frmServer_FormClosing(object sender, FormClosingEventArgs e)
		{
			Console.WriteLine("");

			try
			{
				_server.DataReceived -= _server_DataReceived;
				_server.ClientConnected -= _server_ClientConnected;
				_server.ClientDisconnected -= _server_ClientDisconnected;
				_server.ServerConnected -= _server_ServerConnected;
				_server.ServerDisconnected -= _server_ServerDisonnected;
				_server.IPsReceived += _server_IPsReceived;
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message.ToString());
			}

			_server = null;
		}

		private void btnGetIPs_Click(object sender, EventArgs e)
		{
			//Server getips = (Server)Server.Instance;
			Server getips = new Server();

			getips.IPsReceived += _server_IPsReceived;

			getips.GetIPs();

			getips.IPsReceived -= _server_IPsReceived;

			getips = null;

			btnStartListening.Enabled = true;
		}

		private void btnGetConnectedClientList_Click(object sender, EventArgs e)
		{
			if (_server == null)
				return;

			lstConnectedClients.Items.Clear();

			ArrayList connectedclients = _server.GetConnectedClientList();

			foreach (ClientState c in connectedclients)
			{
				lstConnectedClients.Items.Add(c.CurrentSocketClientID.ToString() + " :: " + c.Nickname);
			}
		}

		private void button1_Click(object sender, EventArgs e)
		{
			
		}

		private void btnComCheckAllClients_Click(object sender, EventArgs e)
		{
			if (_server == null)
				return;

			ClientMessage msg = new ClientMessage();
			msg.Command = "COMCHECK";
			msg.Delimeter = _server.Delimeter;
			msg.MessageToClient = "COMCHECK";


			_server.SendBroadcastMessage(msg);
		}
	
	
	
	}	// end class frmServer

}	// end namespace CDSuite
