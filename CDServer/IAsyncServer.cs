﻿using System;

using System.Collections;
using System.Net;


namespace CDSuite
{
	/// <summary>
	/// Holds the state of a single socket client.
	/// 1 connection to the server = 1 instance of ClientState.
	/// </summary>
	public class ClientState
	{
		/// <summary>
		/// Holds the clients socket.
		/// </summary>
		internal System.Net.Sockets.Socket CurrentSocketClient;
		/// <summary>
		/// The ID of the socket client.
		/// </summary>
		internal int CurrentSocketClientID;
		/// <summary>
		/// Buffer to store the data sent by the socket client.
		/// </summary>
		internal byte[] CurrentSocketDataBuffer = new byte[1024];
		/// <summary>
		/// Primarily used to group connected socket clients.
		/// An example would be the game servers ip that the client is going to be playing on.
		/// </summary>
		public string Channel;

		// examples

		/// <summary>
        /// The socket clients nickname. Ex. [RIP]Hydra.
		/// </summary>
        /// <remarks>Ex. [RIP]Hydra</remarks>
		public string Nickname;
		/// <summary>
		/// EXAMPLE: A currently undefined struct for the game types we support.
		/// </summary>
		public int GameServerType;

		/// <summary>
		/// CStor.
		/// </summary>
		/// <param name="currentsocketclient">Holds the clients socket.</param>
		/// <param name="currentsocketclientID">The ID of the socket client.</param>
		internal ClientState(System.Net.Sockets.Socket currentsocketclient, int currentsocketclientID)
		{
			CurrentSocketClient = currentsocketclient;
			CurrentSocketClientID = currentsocketclientID;
		}
	}

	/// <summary>
	/// Represents a message to send to the socket client.
	/// </summary>
	public struct ClientMessage
	{
        /// <summary>
        /// The command to send to the socket client.
        /// </summary>
		public string Command;
		/// <summary>
		/// The message to send to the socket client.
		/// </summary>
		public string MessageToClient;
		/// <summary>
		/// The delimeter to use for commands and messages.
		/// </summary>
		public string Delimeter;

        /// <summary>
        /// Returns the full command, delimeter and message as a string.
        /// </summary>
        /// <returns>The full command, delimeter and message as a string.</returns>
		public override string ToString()
		{
			return Command + Delimeter + MessageToClient;
		}
	}


	/// <summary>
	/// Delegate for subscribers to know when a socket client has sent a message.
	/// </summary>
	/// <param name="clientID">The ID of the socket client that sent data.</param>
	/// <param name="msgfromclient">The message sent from the socket client.</param>
	public delegate void DataReceivedHandler(int clientID, string msgfromclient);

	/// <summary>
	/// Delegate for subscribers to know when a new socket client connected.
	/// </summary>
	/// <param name="clientID">The ID of the socket client that connected.</param>
	public delegate void ClientConnectedHandler(int clientID);

	/// <summary>
	/// Delegate for subscribers to know which socket client disconnected.
	/// </summary>
	/// <param name="clientID">The ID of the socket client that disconnected.</param>
	public delegate void ClientDisconnectedHandler(int clientID);

	/// <summary>
	/// Delegate for subscribers to know that the Server() instance is listening.
	/// </summary>
	public delegate void ServerConnectedHandler();

	/// <summary>
	/// Delegate for subscribers to know that the Server() instance is not listening.
	/// </summary>
	public delegate void ServerDisconnectedHandler();

	/// <summary>
	/// Delegate for subscribers to know that the Server() instance has retrieved its local IPs.
	/// </summary>
	public delegate void IPsRetrievedHandler(ArrayList ips);



	/// <summary>
	/// Interface for an arbitrary Async Server.
	/// </summary>
	public interface IAsyncServer : IDisposable
	{
		/// <summary>
		/// Event fired when we've received data from a socket client.
		/// </summary>
		event DataReceivedHandler DataReceived;
		/// <summary>
		/// Event fired when we've identified all our local endpoints.
		/// </summary>
		event IPsRetrievedHandler IPsReceived;
		/// <summary>
		/// Event fired when a socket client connects.
		/// </summary>
		event ClientConnectedHandler ClientConnected;
		/// <summary>
		/// Event fired when a socket client disconnects.
		/// </summary>
		event ClientDisconnectedHandler ClientDisconnected;
		/// <summary>
		/// Event fired when the server starts listening for socket clients.
		/// </summary>
		event ServerConnectedHandler ServerConnected;
		/// <summary>
		/// Event fired when the server stops listening for clients. Means we are not accepting any new client connections.
		/// </summary>
		event ServerDisconnectedHandler ServerDisconnected;
		/// <summary>
		/// Callback function which will be invoked when the socket detects any client writing data on the stream.
		/// </summary>
		/// <param name="asyn">asyn.AsyncState holds a reference to a ClientState object.</param>
		void OnDataReceived(IAsyncResult asyn);
		/// <summary>
		/// Signal to start the servers listener socket on a particular EndPoint.
		/// </summary>
		void StartListeningForClients(IPEndPoint localEndPoint);
		/// <summary>
		/// Returns if we are in a listening state.
		/// </summary>
		/// <returns>True if we are listening, otherwise False.</returns>
		bool AreWeListening();
		/// <summary>
		/// Signal to stop the servers listener socket. Does not disconnect currently connected clients.
		/// </summary>
		void StopListeningForClients();
		/// <summary>
		/// Check if a socket client is connected.
		/// </summary>
		/// <param name="clientID">The ID of the client.</param>
		/// <returns>True if connected, otherwise False.</returns>
		bool IsClientConnected(int clientID);
		/// <summary>
		/// Callback function which will be invoked when a socket client connects.
		/// </summary>
		/// <param name="asyn">asyn.AsyncState holds a reference to a Socket object.</param>
		void OnClientConnect(IAsyncResult asyn);
		/// <summary>
		/// Start waiting for data from the socket client.
		/// </summary>
		/// <param name="clientsocket"></param>
		/// <param name="clientstate"></param>
		void WaitForDataFromClient(System.Net.Sockets.Socket clientsocket, ClientState clientstate);
		/// <summary>
		/// Sends a message to the specified socket client.
		/// </summary>
		/// <param name="msgtoclient">The message to send to the client.</param>
		/// <param name="clientID">The ID of the client to send the message to.</param>
		void SendMessageToClient(ClientMessage msgtoclient, int clientID);
		/// <summary>
		/// Sends a message to all connected socket clients.
		/// </summary>
		/// <param name="messagetoclient">The message to send to all the clients.</param>
		void SendBroadcastMessage(ClientMessage messagetoclient);
		/// <summary>
		/// Disconnects all currently connected socket clients.
		/// </summary>
		void DisconnectAllClients();
		/// <summary>
		/// Gets a list of available host IPs.
		/// </summary>
		void GetIPs();
		/// <summary>
		/// Returns a list of the connected socket clients.
		/// </summary>
		/// <returns></returns>
		ArrayList GetConnectedClientList();
		/// <summary>
		/// Updates the ClientState of a particular socket client.
		/// </summary>
		/// <param name="newclientstate">The ClientState object that holds the new info.</param>
		/// <param name="clientID">The ID of the client to update.</param>
		void UpdateClientState(ClientState newclientstate, int clientID);

	}	// end  interface IAsyncServer

}	// end namespace CDSuite
