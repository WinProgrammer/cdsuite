﻿namespace CDSuite
{
	partial class frmServer
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnDisconnectAllClients = new System.Windows.Forms.Button();
			this.txtClientID = new System.Windows.Forms.TextBox();
			this.btnStopListening = new System.Windows.Forms.Button();
			this.txtMessage = new System.Windows.Forms.TextBox();
			this.btnSendMessage = new System.Windows.Forms.Button();
			this.txtReceive = new System.Windows.Forms.TextBox();
			this.btnStartListening = new System.Windows.Forms.Button();
			this.btnSendBroadcastMessage = new System.Windows.Forms.Button();
			this.cboIPs = new System.Windows.Forms.ComboBox();
			this.txtPort = new System.Windows.Forms.TextBox();
			this.lblPort = new System.Windows.Forms.Label();
			this.lblIPs = new System.Windows.Forms.Label();
			this.btnGetIPs = new System.Windows.Forms.Button();
			this.lstConnectedClients = new System.Windows.Forms.ListBox();
			this.btnGetConnectedClientList = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.btnComCheckAllClients = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// btnDisconnectAllClients
			// 
			this.btnDisconnectAllClients.Location = new System.Drawing.Point(419, 15);
			this.btnDisconnectAllClients.Name = "btnDisconnectAllClients";
			this.btnDisconnectAllClients.Size = new System.Drawing.Size(120, 23);
			this.btnDisconnectAllClients.TabIndex = 20;
			this.btnDisconnectAllClients.Text = "Disconnect All Clients";
			this.btnDisconnectAllClients.UseVisualStyleBackColor = true;
			this.btnDisconnectAllClients.Click += new System.EventHandler(this.btnDisconnectAllClients_Click);
			// 
			// txtClientID
			// 
			this.txtClientID.Location = new System.Drawing.Point(13, 286);
			this.txtClientID.Name = "txtClientID";
			this.txtClientID.Size = new System.Drawing.Size(100, 20);
			this.txtClientID.TabIndex = 19;
			this.txtClientID.Text = "1";
			// 
			// btnStopListening
			// 
			this.btnStopListening.Enabled = false;
			this.btnStopListening.Location = new System.Drawing.Point(12, 47);
			this.btnStopListening.Name = "btnStopListening";
			this.btnStopListening.Size = new System.Drawing.Size(113, 23);
			this.btnStopListening.TabIndex = 18;
			this.btnStopListening.Text = "Stop Listening";
			this.btnStopListening.UseVisualStyleBackColor = true;
			this.btnStopListening.Click += new System.EventHandler(this.btnStopListening_Click);
			// 
			// txtMessage
			// 
			this.txtMessage.Location = new System.Drawing.Point(135, 287);
			this.txtMessage.Name = "txtMessage";
			this.txtMessage.Size = new System.Drawing.Size(258, 20);
			this.txtMessage.TabIndex = 17;
			// 
			// btnSendMessage
			// 
			this.btnSendMessage.Location = new System.Drawing.Point(3, 319);
			this.btnSendMessage.Name = "btnSendMessage";
			this.btnSendMessage.Size = new System.Drawing.Size(133, 23);
			this.btnSendMessage.TabIndex = 16;
			this.btnSendMessage.Text = "Send Message";
			this.btnSendMessage.UseVisualStyleBackColor = true;
			this.btnSendMessage.Click += new System.EventHandler(this.btnSendMessage_Click);
			// 
			// txtReceive
			// 
			this.txtReceive.Location = new System.Drawing.Point(12, 134);
			this.txtReceive.Multiline = true;
			this.txtReceive.Name = "txtReceive";
			this.txtReceive.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.txtReceive.Size = new System.Drawing.Size(381, 126);
			this.txtReceive.TabIndex = 15;
			// 
			// btnStartListening
			// 
			this.btnStartListening.Enabled = false;
			this.btnStartListening.Location = new System.Drawing.Point(12, 15);
			this.btnStartListening.Name = "btnStartListening";
			this.btnStartListening.Size = new System.Drawing.Size(113, 23);
			this.btnStartListening.TabIndex = 14;
			this.btnStartListening.Text = "Start Listening";
			this.btnStartListening.UseVisualStyleBackColor = true;
			this.btnStartListening.Click += new System.EventHandler(this.btnStartListening_Click);
			// 
			// btnSendBroadcastMessage
			// 
			this.btnSendBroadcastMessage.Location = new System.Drawing.Point(142, 319);
			this.btnSendBroadcastMessage.Name = "btnSendBroadcastMessage";
			this.btnSendBroadcastMessage.Size = new System.Drawing.Size(148, 23);
			this.btnSendBroadcastMessage.TabIndex = 21;
			this.btnSendBroadcastMessage.Text = "Send Broadcast Message";
			this.btnSendBroadcastMessage.UseVisualStyleBackColor = true;
			this.btnSendBroadcastMessage.Click += new System.EventHandler(this.btnSendBroadcastMessage_Click);
			// 
			// cboIPs
			// 
			this.cboIPs.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboIPs.FormattingEnabled = true;
			this.cboIPs.Location = new System.Drawing.Point(201, 49);
			this.cboIPs.Name = "cboIPs";
			this.cboIPs.Size = new System.Drawing.Size(182, 21);
			this.cboIPs.TabIndex = 25;
			// 
			// txtPort
			// 
			this.txtPort.Location = new System.Drawing.Point(204, 76);
			this.txtPort.Name = "txtPort";
			this.txtPort.Size = new System.Drawing.Size(58, 20);
			this.txtPort.TabIndex = 24;
			this.txtPort.Text = "8000";
			// 
			// lblPort
			// 
			this.lblPort.AutoSize = true;
			this.lblPort.Location = new System.Drawing.Point(160, 79);
			this.lblPort.Name = "lblPort";
			this.lblPort.Size = new System.Drawing.Size(26, 13);
			this.lblPort.TabIndex = 23;
			this.lblPort.Text = "Port";
			// 
			// lblIPs
			// 
			this.lblIPs.AutoSize = true;
			this.lblIPs.Location = new System.Drawing.Point(160, 49);
			this.lblIPs.Name = "lblIPs";
			this.lblIPs.Size = new System.Drawing.Size(22, 13);
			this.lblIPs.TabIndex = 22;
			this.lblIPs.Text = "IPs";
			// 
			// btnGetIPs
			// 
			this.btnGetIPs.Location = new System.Drawing.Point(166, 15);
			this.btnGetIPs.Name = "btnGetIPs";
			this.btnGetIPs.Size = new System.Drawing.Size(118, 23);
			this.btnGetIPs.TabIndex = 26;
			this.btnGetIPs.Text = "Get IPs";
			this.btnGetIPs.UseVisualStyleBackColor = true;
			this.btnGetIPs.Click += new System.EventHandler(this.btnGetIPs_Click);
			// 
			// lstConnectedClients
			// 
			this.lstConnectedClients.FormattingEnabled = true;
			this.lstConnectedClients.Location = new System.Drawing.Point(405, 120);
			this.lstConnectedClients.Name = "lstConnectedClients";
			this.lstConnectedClients.Size = new System.Drawing.Size(134, 147);
			this.lstConnectedClients.TabIndex = 27;
			// 
			// btnGetConnectedClientList
			// 
			this.btnGetConnectedClientList.Location = new System.Drawing.Point(405, 91);
			this.btnGetConnectedClientList.Name = "btnGetConnectedClientList";
			this.btnGetConnectedClientList.Size = new System.Drawing.Size(134, 23);
			this.btnGetConnectedClientList.TabIndex = 28;
			this.btnGetConnectedClientList.Text = "Get connecetd client list";
			this.btnGetConnectedClientList.UseVisualStyleBackColor = true;
			this.btnGetConnectedClientList.Click += new System.EventHandler(this.btnGetConnectedClientList_Click);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(478, 334);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 29;
			this.button1.Text = "button1";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// btnComCheckAllClients
			// 
			this.btnComCheckAllClients.Location = new System.Drawing.Point(297, 318);
			this.btnComCheckAllClients.Name = "btnComCheckAllClients";
			this.btnComCheckAllClients.Size = new System.Drawing.Size(138, 23);
			this.btnComCheckAllClients.TabIndex = 30;
			this.btnComCheckAllClients.Text = "ComCheck all clients";
			this.btnComCheckAllClients.UseVisualStyleBackColor = true;
			this.btnComCheckAllClients.Click += new System.EventHandler(this.btnComCheckAllClients_Click);
			// 
			// frmServer
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(565, 369);
			this.Controls.Add(this.btnComCheckAllClients);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.btnGetConnectedClientList);
			this.Controls.Add(this.lstConnectedClients);
			this.Controls.Add(this.btnGetIPs);
			this.Controls.Add(this.cboIPs);
			this.Controls.Add(this.txtPort);
			this.Controls.Add(this.lblPort);
			this.Controls.Add(this.lblIPs);
			this.Controls.Add(this.btnSendBroadcastMessage);
			this.Controls.Add(this.btnDisconnectAllClients);
			this.Controls.Add(this.txtClientID);
			this.Controls.Add(this.btnStopListening);
			this.Controls.Add(this.txtMessage);
			this.Controls.Add(this.btnSendMessage);
			this.Controls.Add(this.txtReceive);
			this.Controls.Add(this.btnStartListening);
			this.Name = "frmServer";
			this.Text = "Server";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmServer_FormClosing);
			this.Load += new System.EventHandler(this.frmServer_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnDisconnectAllClients;
		private System.Windows.Forms.TextBox txtClientID;
		private System.Windows.Forms.Button btnStopListening;
		private System.Windows.Forms.TextBox txtMessage;
		private System.Windows.Forms.Button btnSendMessage;
		private System.Windows.Forms.TextBox txtReceive;
		private System.Windows.Forms.Button btnStartListening;
		private System.Windows.Forms.Button btnSendBroadcastMessage;
		private System.Windows.Forms.ComboBox cboIPs;
		private System.Windows.Forms.TextBox txtPort;
		private System.Windows.Forms.Label lblPort;
		private System.Windows.Forms.Label lblIPs;
		private System.Windows.Forms.Button btnGetIPs;
		private System.Windows.Forms.ListBox lstConnectedClients;
		private System.Windows.Forms.Button btnGetConnectedClientList;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button btnComCheckAllClients;
	}
}

