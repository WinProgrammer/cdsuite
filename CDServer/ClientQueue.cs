﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace CDSuite
{
	/// <summary>
	/// Delegate for when an item is added to the queue.
	/// </summary>
	public delegate void ItemEnqueuedHandler();

	/// <summary>
	/// A regular Queue class with an added event for when somethings enqueued.
	/// </summary>
	public class ClientQueue : Queue
	{
		/// <summary>
		/// Event fired when an item is added to the queue.
		/// </summary>
		public event ItemEnqueuedHandler ItemEnqueued;


		/// <summary>
		/// Enques an object in the ClientQueue.
		/// </summary>
		/// <param name="obj">What to enqueue.</param>
		public override void Enqueue(object obj)
		{
			base.Enqueue(obj);

			// fire
			if (ItemEnqueued != null)
				ItemEnqueued.Invoke();
		}

	}	// end public class ClientQueue

}	// end namespace CDSuite
