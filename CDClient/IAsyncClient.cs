﻿using System;

namespace CDSuite
{
    /// <summary>
    /// Keep track of our own state
    /// </summary>
	internal class ClientState
	{
		internal System.Net.Sockets.Socket CurrentSocket;
		internal byte[] DataBuffer = new byte[1024];
	}


	/// <summary>
	/// Represents a message to send to the server.
	/// </summary>
	public struct ServerMessage
	{
		/// <summary>
		/// The command to send to the server.
		/// </summary>
		public string Command;
		/// <summary>
		/// The message to send to the server.
		/// </summary>
		public string MessageToServer;
		/// <summary>
		/// The delimeter to use for commands and messages.
		/// </summary>
		public string Delimeter;

		/// <summary>
		/// Returns the full command, delim and message as a string.
		/// </summary>
		/// <returns>The full command, delim and message as a string.</returns>
		public override string ToString()
		{
			return Command + Delimeter + MessageToServer;
		}
	}


	/// <summary>
	/// Delegate for when we connect to a server.
	/// </summary>
	public delegate void ConnectedToServerHandler();
	/// <summary>
	/// Delegate for when we disconnect from a server.
	/// </summary>
	public delegate void DisconnectedFromServerHandler();
	/// <summary>
	/// Delegate for when we receive data from the server.
	/// </summary>
	/// <param name="msg">The message from the server.</param>
	public delegate void DataReceivedHandler(string msg);




	/// <summary>
	/// Interface for an asynchronous socket client.
	/// </summary>
	public interface IAsyncClient : IDisposable
	{
		/// <summary>
		/// Event fired when we connect to the server.
		/// </summary>
		event ConnectedToServerHandler ConnectedToServer;
		/// <summary>
		/// Event fired when we've disconnected from the server.
		/// </summary>
		event DisconnectedFromServerHandler DisconnectedFromServer;
		/// <summary>
		/// Event fired when we've received data from the server.
		/// </summary>
		event DataReceivedHandler DataReceived;
		/// <summary>
		/// Connect to the server.
		/// </summary>
		/// <returns>True if successful, otherwise False.</returns>
		bool ConnectToServer();
		/// <summary>
		/// Disconnect from the server.
		/// </summary>
		void DisconnectFromServer();
		/// <summary>
		/// Start waiting for data from server.
		/// </summary>
		void WaitForDataFromServer();
		/// <summary>
		/// Handles receiving messages from server.
		/// </summary>
		/// <param name="asyn"></param>
		void OnDataReceived(IAsyncResult asyn);
		/// <summary>
		/// Sends a message to the server.
		/// </summary>
		/// <param name="msg">The message to send.</param>
		void SendMessageToServer(ServerMessage msg);


	
	}	// end interface IAsyncClient

}	// end namespace AsyncClient
