﻿namespace CDSuite
{
	partial class frmClient
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnDisconnect = new System.Windows.Forms.Button();
			this.txtMessage = new System.Windows.Forms.TextBox();
			this.btnSendMessage = new System.Windows.Forms.Button();
			this.btnConnectToServer = new System.Windows.Forms.Button();
			this.txtReceive = new System.Windows.Forms.TextBox();
			this.button1 = new System.Windows.Forms.Button();
			this.txtServerPort = new System.Windows.Forms.TextBox();
			this.lblPort = new System.Windows.Forms.Label();
			this.lblServerIP = new System.Windows.Forms.Label();
			this.txtServerIP = new System.Windows.Forms.TextBox();
			this.txtGameServerIP = new System.Windows.Forms.TextBox();
			this.lblGameServerIP = new System.Windows.Forms.Label();
			this.txtNickname = new System.Windows.Forms.TextBox();
			this.lblNickname = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// btnDisconnect
			// 
			this.btnDisconnect.Enabled = false;
			this.btnDisconnect.Location = new System.Drawing.Point(244, 41);
			this.btnDisconnect.Name = "btnDisconnect";
			this.btnDisconnect.Size = new System.Drawing.Size(124, 23);
			this.btnDisconnect.TabIndex = 14;
			this.btnDisconnect.Text = "Disconnect";
			this.btnDisconnect.UseVisualStyleBackColor = true;
			this.btnDisconnect.Click += new System.EventHandler(this.btnDisconnect_Click);
			// 
			// txtMessage
			// 
			this.txtMessage.Location = new System.Drawing.Point(46, 414);
			this.txtMessage.Name = "txtMessage";
			this.txtMessage.Size = new System.Drawing.Size(457, 20);
			this.txtMessage.TabIndex = 13;
			// 
			// btnSendMessage
			// 
			this.btnSendMessage.Location = new System.Drawing.Point(46, 440);
			this.btnSendMessage.Name = "btnSendMessage";
			this.btnSendMessage.Size = new System.Drawing.Size(133, 23);
			this.btnSendMessage.TabIndex = 12;
			this.btnSendMessage.Text = "Send Message";
			this.btnSendMessage.UseVisualStyleBackColor = true;
			this.btnSendMessage.Click += new System.EventHandler(this.btnSendMessage_Click);
			// 
			// btnConnectToServer
			// 
			this.btnConnectToServer.Location = new System.Drawing.Point(244, 12);
			this.btnConnectToServer.Name = "btnConnectToServer";
			this.btnConnectToServer.Size = new System.Drawing.Size(124, 23);
			this.btnConnectToServer.TabIndex = 10;
			this.btnConnectToServer.Text = "Connect to Server";
			this.btnConnectToServer.UseVisualStyleBackColor = true;
			this.btnConnectToServer.Click += new System.EventHandler(this.btnConnectToServer_Click);
			// 
			// txtReceive
			// 
			this.txtReceive.Location = new System.Drawing.Point(46, 243);
			this.txtReceive.Multiline = true;
			this.txtReceive.Name = "txtReceive";
			this.txtReceive.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.txtReceive.Size = new System.Drawing.Size(457, 150);
			this.txtReceive.TabIndex = 16;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(244, 73);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 17;
			this.button1.Text = "button1";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// txtServerPort
			// 
			this.txtServerPort.Location = new System.Drawing.Point(78, 38);
			this.txtServerPort.Name = "txtServerPort";
			this.txtServerPort.Size = new System.Drawing.Size(100, 20);
			this.txtServerPort.TabIndex = 27;
			this.txtServerPort.Text = "8000";
			// 
			// lblPort
			// 
			this.lblPort.AutoSize = true;
			this.lblPort.Location = new System.Drawing.Point(21, 41);
			this.lblPort.Name = "lblPort";
			this.lblPort.Size = new System.Drawing.Size(26, 13);
			this.lblPort.TabIndex = 26;
			this.lblPort.Text = "Port";
			// 
			// lblServerIP
			// 
			this.lblServerIP.AutoSize = true;
			this.lblServerIP.Location = new System.Drawing.Point(21, 15);
			this.lblServerIP.Name = "lblServerIP";
			this.lblServerIP.Size = new System.Drawing.Size(51, 13);
			this.lblServerIP.TabIndex = 25;
			this.lblServerIP.Text = "Server IP";
			// 
			// txtServerIP
			// 
			this.txtServerIP.Location = new System.Drawing.Point(78, 12);
			this.txtServerIP.Name = "txtServerIP";
			this.txtServerIP.Size = new System.Drawing.Size(100, 20);
			this.txtServerIP.TabIndex = 28;
			// 
			// txtGameServerIP
			// 
			this.txtGameServerIP.Location = new System.Drawing.Point(100, 70);
			this.txtGameServerIP.Name = "txtGameServerIP";
			this.txtGameServerIP.Size = new System.Drawing.Size(96, 20);
			this.txtGameServerIP.TabIndex = 30;
			this.txtGameServerIP.Text = "255.255.255.255";
			// 
			// lblGameServerIP
			// 
			this.lblGameServerIP.AutoSize = true;
			this.lblGameServerIP.Location = new System.Drawing.Point(12, 73);
			this.lblGameServerIP.Name = "lblGameServerIP";
			this.lblGameServerIP.Size = new System.Drawing.Size(82, 13);
			this.lblGameServerIP.TabIndex = 29;
			this.lblGameServerIP.Text = "Game Server IP";
			// 
			// txtNickname
			// 
			this.txtNickname.Location = new System.Drawing.Point(100, 96);
			this.txtNickname.Name = "txtNickname";
			this.txtNickname.Size = new System.Drawing.Size(96, 20);
			this.txtNickname.TabIndex = 32;
			this.txtNickname.Text = "Hydra";
			// 
			// lblNickname
			// 
			this.lblNickname.AutoSize = true;
			this.lblNickname.Location = new System.Drawing.Point(17, 99);
			this.lblNickname.Name = "lblNickname";
			this.lblNickname.Size = new System.Drawing.Size(55, 13);
			this.lblNickname.TabIndex = 31;
			this.lblNickname.Text = "Nickname";
			// 
			// frmClient
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(800, 595);
			this.Controls.Add(this.txtNickname);
			this.Controls.Add(this.lblNickname);
			this.Controls.Add(this.txtGameServerIP);
			this.Controls.Add(this.lblGameServerIP);
			this.Controls.Add(this.txtServerIP);
			this.Controls.Add(this.txtServerPort);
			this.Controls.Add(this.lblPort);
			this.Controls.Add(this.lblServerIP);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.txtReceive);
			this.Controls.Add(this.btnDisconnect);
			this.Controls.Add(this.txtMessage);
			this.Controls.Add(this.btnSendMessage);
			this.Controls.Add(this.btnConnectToServer);
			this.Name = "frmClient";
			this.Text = "Client";
			this.Load += new System.EventHandler(this.frmClient_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnDisconnect;
		private System.Windows.Forms.TextBox txtMessage;
		private System.Windows.Forms.Button btnSendMessage;
		private System.Windows.Forms.Button btnConnectToServer;
		private System.Windows.Forms.TextBox txtReceive;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox txtServerPort;
		private System.Windows.Forms.Label lblPort;
		private System.Windows.Forms.Label lblServerIP;
		private System.Windows.Forms.TextBox txtServerIP;
		private System.Windows.Forms.TextBox txtGameServerIP;
		private System.Windows.Forms.Label lblGameServerIP;
		private System.Windows.Forms.TextBox txtNickname;
		private System.Windows.Forms.Label lblNickname;
	}
}

