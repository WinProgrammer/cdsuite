﻿using System;

using System.Net;
using System.Net.Sockets;

namespace CDSuite
{
	/// <summary>
	/// Represents a single client.
	/// </summary>
	public sealed class Client : IAsyncClient
	{

		#region Local Fields

		byte[] _dataBuffer = new byte[10];
		IAsyncResult _result;
		private AsyncCallback _waitingSocketCallBack;
		private Socket _clientSocket;

		#endregion



		/// <summary>
		/// The IP of the server to connect to. Can only be set in the CStor.
		/// </summary>
		public readonly string SERVERIP = "127.0.0.1";

		/// <summary>
		/// The port to use to connect to a server. Can only be set in the CStor.
		/// </summary>
		public readonly int PORT = 8000;

		/// <summary>
		/// The delimeter to use for splitting messages from commands.
		/// </summary>
		public string Delimeter = "||";

		/// <summary>
		/// CStor.
		/// </summary>
		public Client()
		{
		}
		
		/// <summary>
		/// CStor.
		/// </summary>
		/// <param name="serverip">The IP of the server to connect to.</param>
		/// <param name="serverport">The port to connect to.</param>
		public Client(string serverip, int serverport)
		{
			SERVERIP = serverip;
			PORT = serverport;
		}



		#region IAsyncClient Members

		/// <summary>
		/// Event fired when we connect to the server.
		/// </summary>
		public event ConnectedToServerHandler ConnectedToServer;

		/// <summary>
		/// Event fired when we've disconnected from the server.
		/// </summary>
		public event DisconnectedFromServerHandler DisconnectedFromServer;

		/// <summary>
		/// Event fired when we've received data from the server.
		/// </summary>
		public event DataReceivedHandler DataReceived;





		/// <summary>
		/// Connect to a server.
		/// </summary>
		/// <returns>True if successful, otherwise False.</returns>
		public bool ConnectToServer()
		{
			try
			{
				// Create the socket instance using IP4
				_clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

				// Establish the endpoint to the server.
				IPAddress serveripAddress = IPAddress.Parse(SERVERIP);
				IPEndPoint serverEndPoint = new IPEndPoint(serveripAddress, PORT);

				// Connect to the server
				_clientSocket.Connect(serverEndPoint);
				if (_clientSocket.Connected)
				{
					// Wait for data from server asynchronously 
					WaitForDataFromServer();
				}
			}
			catch (SocketException se)
			{
				throw se;
			}

			if (ConnectedToServer != null)
				ConnectedToServer.Invoke();

			return true;
		}

		/// <summary>
		/// Disconnect from the server.
		/// </summary>
		public void DisconnectFromServer()
		{
			if (_clientSocket != null)
			{
				_clientSocket.Close();
				_clientSocket = null;
			}

			if (DisconnectedFromServer != null)
				DisconnectedFromServer.Invoke();
		}



		/// <summary>
		/// Start waiting for data from server.
		/// </summary>
		public void WaitForDataFromServer()
		{
			try
			{
				if (_waitingSocketCallBack == null)
				{
					_waitingSocketCallBack = new AsyncCallback(OnDataReceived);
				}

				if (_clientSocket == null)
					return;

				ClientState cs = new ClientState();
				cs.CurrentSocket = _clientSocket;
				// Start listening to the data asynchronously
				_result = _clientSocket.BeginReceive(cs.DataBuffer, 0, cs.DataBuffer.Length, SocketFlags.None, _waitingSocketCallBack, cs);
			}
			catch (SocketException se)
			{
				throw se;
			}

		}

		/// <summary>
		/// Handles receiving messages from server.
		/// </summary>
		/// <param name="asyn">asyn will hold a ClientState reference.</param>
		public void OnDataReceived(IAsyncResult asyn)
		{
			ClientState client = (ClientState)asyn.AsyncState;
			try
			{
				// Complete the async call which will return the number of characters written to the stream by the server
				int numberofbytesfromserver = client.CurrentSocket.EndReceive(asyn);

				// Extract the characters as a buffer
				char[] servermessagebuffer = new char[numberofbytesfromserver + 1];
				System.Text.Decoder decoder = System.Text.Encoding.UTF8.GetDecoder();
				int charLen = decoder.GetChars(client.DataBuffer, 0, numberofbytesfromserver, servermessagebuffer, 0);
				string messagefromserver = new System.String(servermessagebuffer);
				// strip any possible residue
				messagefromserver = messagefromserver.Replace("\r\n\0", "");

				if (DataReceived != null)
					DataReceived.Invoke(messagefromserver);

				// Continue waiting for data from the server
				WaitForDataFromServer();
			}
			catch (Exception ex)
			{
				// raise event to notify we disconnected from server
				if (DisconnectedFromServer != null)
					DisconnectedFromServer.Invoke();
			}
		}	


		/// <summary>
		/// Sends a message to the server.
		/// </summary>
		/// <param name="msgtoserver">The message to send.</param>
		public void SendMessageToServer(ServerMessage msgtoserver)
		{
			if (_clientSocket == null)
				return;

			try
			{
				NetworkStream networkStream = new NetworkStream(_clientSocket);
				System.IO.StreamWriter streamWriter = new System.IO.StreamWriter(networkStream);
				streamWriter.WriteLine(msgtoserver.ToString());
				streamWriter.Flush();
			}
			catch (SocketException se)
			{
				// ignore
				//throw se;
			}	
		}



		#endregion

		#region IDisposable Members

		/// <summary>
		/// 
		/// </summary>
		public void Dispose()
		{
			DisconnectFromServer();
		}

		#endregion
	
	
	}	// end class Client

}	// end namespace CDSuite
