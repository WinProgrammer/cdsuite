﻿using System;
using System.Windows.Forms;
using System.Collections;
using System.Net;

namespace CDSuite
{
	/// <summary>
	/// 
	/// </summary>
	public partial class frmClient : Form
	{
		Client _client = null;

		// This delegate enables asynchronous calls for setting the text property of txtReceive
		delegate void SetTextCallback(string text);

		private void SetText(string text)
		{
			if (this.txtReceive.InvokeRequired)
			{
				SetTextCallback d = new SetTextCallback(SetText);
				this.Invoke(d, new object[] { text });
			}
			else
			{
				this.txtReceive.Text += text;
			}
		}



		delegate void SwapEnabledStateCallback(Control ctl);
		/// <summary>
		/// Swaps the Enabled state of a form control.
		/// </summary>
		/// <param name="ctl">The form control to invert the enabled state on.</param>
		private void SwapEnabledState(Control ctl)
		{
			if (ctl.InvokeRequired)
			{
				SwapEnabledStateCallback d = new SwapEnabledStateCallback(SwapEnabledState);
				this.Invoke(d, new object[] { ctl });
			}
			else
			{
				ctl.Enabled = !ctl.Enabled;
			}
		}




		/// <summary>
		/// CStor.
		/// </summary>
		public frmClient()
		{
			InitializeComponent();
		}




		private void btnConnectToServer_Click(object sender, EventArgs e)
		{
			_client = new Client(txtServerIP.Text, int.Parse(txtServerPort.Text));

			_client.ConnectedToServer += _client_ConnectedToServer;
			_client.DisconnectedFromServer += _client_DisconnectedFromServer;
			_client.DataReceived += _client_DataReceived;

			bool ret = _client.ConnectToServer();

			if (ret)
			{
				SwapEnabledState(btnConnectToServer);
				SwapEnabledState(btnDisconnect);
			}
			else
			{
				MessageBox.Show("Could not connect to server.");
			}
		}

		
		/// <summary>
		/// Fired when the server has sent data to us.
		/// </summary>
		/// <param name="msgfromserver">The message the server sent.</param>
		void _client_DataReceived(string msgfromserver)
		{
			//SetText(msgfromserver + "\r\n");

			// decode the message from the server
			string[] tmp = msgfromserver.Split(new string[] { _client.Delimeter }, StringSplitOptions.None);
			string cmd = tmp[0];
			string msg = "";

			if (tmp.GetLength(0) > 1)
				msg = tmp[1];

			ServerMessage msgtoserver;

			// interpret the message from the server
			switch (cmd)
			{
				case "COMCHECK":
					if (_client == null)
						break;
					// comcheck
					msgtoserver = new ServerMessage();
					msgtoserver.Command = "COMCHECK";
					msgtoserver.Delimeter = _client.Delimeter;
					msgtoserver.MessageToServer = DateTime.Now.ToString();

					_client.SendMessageToServer(msgtoserver);

					break;

				case "CHANNEL":
					if (_client == null)
						break;
					// let the server know which channel we want
					msgtoserver = new ServerMessage();
					msgtoserver.Command = "CHANNEL";
					msgtoserver.Delimeter = _client.Delimeter.ToString();
					msgtoserver.MessageToServer = txtGameServerIP.Text;

					_client.SendMessageToServer(msgtoserver);
					break;

				case "NICKNAME":
					if (_client == null)
						break;
					// let the server know which nickname we want
					msgtoserver = new ServerMessage();
					msgtoserver.Command = "NICKNAME";
					msgtoserver.Delimeter = _client.Delimeter.ToString();
					msgtoserver.MessageToServer = txtNickname.Text;

					_client.SendMessageToServer(msgtoserver);

					break;

				case "SAY":
					SetText("Server says: " + msg + "\r\n");

					break;

				default:
					break;
			}

		}

		
		/// <summary>
		/// Fired when we've disconnected from the server.
		/// </summary>
		void _client_DisconnectedFromServer()
		{
			SetText("Disconnected from server\r\n");

			SwapEnabledState(btnConnectToServer);
			SwapEnabledState(btnDisconnect);
		}

		/// <summary>
		/// Fired when we've connecetd to the server.
		/// </summary>
		void _client_ConnectedToServer()
		{
			SetText("Connected to server\r\n");
		}

		private void btnDisconnect_Click(object sender, EventArgs e)
		{
			if (_client == null)
				return;

			try
			{
				_client.ConnectedToServer -= _client_ConnectedToServer;
				_client.DisconnectedFromServer -= _client_DisconnectedFromServer;
				_client.DataReceived -= _client_DataReceived;
			}
			catch { }

			_client.DisconnectFromServer();

			SwapEnabledState(btnConnectToServer);
			SwapEnabledState(btnDisconnect);

			_client.Dispose();
			_client = null;
		}

		private void btnSendMessage_Click(object sender, EventArgs e)
		{
			if (_client == null)
				return;

			ServerMessage msgtoserver = new ServerMessage();
			msgtoserver.Command = "SAY";
			msgtoserver.Delimeter = _client.Delimeter.ToString();
			msgtoserver.MessageToServer = txtMessage.Text;

			_client.SendMessageToServer(msgtoserver);
		}

		private void frmClient_Load(object sender, EventArgs e)
		{
			GetIPs();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			Hashtable h = ProcessManager.GetCurrentProcessList();




			////var path = @"c:\windows\system32\mscoree.dll";
			////var path = @"c:\windows\system32\gdi32.dll";
			//var path = @"c:\windows\system32\wsock32.dll";
			//var hLib = LoadLibraryEx(path, 0, DONT_RESOLVE_DLL_REFERENCES | LOAD_IGNORE_CODE_AUTHZ_LEVEL);
			//TestImports(hLib, true);

			Console.WriteLine("");
		}

		/// <summary>
		/// Gets a list of available IPs on this host.
		/// </summary>
		public void GetIPs()
		{
			IPHostEntry localhost = Dns.GetHostEntry(Dns.GetHostName());

			// TODO: Don't use this
			txtServerIP.Text = localhost.AddressList[2].ToString();

			//ArrayList ips = new ArrayList();

			//// add the available ips to the list
			//foreach (IPAddress ip in localhost.AddressList)
			//{
			//	//if(ip.AddressFamily)
			//	ips.Add(ip.ToString());

			//}

			//// add loopback
			//ips.Add("127.0.0.1");
		}




	
	}	// end class frmClient

}	// end namespace CDSuite
