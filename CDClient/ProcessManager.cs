﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections;
using System.Diagnostics;

namespace CDSuite
{
	/// <summary>
	/// Handles all process related functions.
	/// </summary>
	public static class ProcessManager
	{
		private static Hashtable procs;


		/// <summary>
		/// Gets a list of running processes.
		/// </summary>
		/// <returns>A Hashtable. The Key is the process id and the Value is the process name.</returns>
		public static Hashtable GetCurrentProcessList()
		{
			procs = new Hashtable();

			Process[] processlist = Process.GetProcesses();

			foreach(Process theprocess in processlist)
			{
				// exclude System and Idle, (0, 4)
				if (theprocess.Id == 0 || theprocess.Id == 4)
					continue;

				procs.Add(theprocess.Id, theprocess.ProcessName);
				//Console.WriteLine(theprocess.Id.ToString() + "\t" + theprocess.ProcessName);
			}

			return procs;
		}


	}	// end static class ProcessManager

}	// end namespace CDSuite
